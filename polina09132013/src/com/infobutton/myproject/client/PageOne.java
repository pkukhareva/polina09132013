package com.infobutton.myproject.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.DOMException;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;



public class PageOne extends Composite {
	private HorizontalPanel hpanel1 = new HorizontalPanel();
	private VerticalPanel vpanel1 = new VerticalPanel();
	private HorizontalPanel hpanel2= new HorizontalPanel();	
    private TextArea textarea1 = new TextArea();	
	private Label mylbl1;
    private Button button1;
    private Button button2; 
    private Button button3; 
    private Button button4;     
	private VerticalPanel vPanel2 = new VerticalPanel();
	private Label mylbl3;
	private Image img2;
    public static String  url1;

    
	public PageOne(){
		initWidget(this.hpanel1);
    	hpanel1.addStyleName("gwt-hpanel1-style");	
		
		Image img1= new Image("/IMAGES1/IMG_2878.JPG");
		vpanel1.add(img1);
		
		this.mylbl1=new Label("Electronical Health Record");
		vpanel1.add(this.mylbl1);
	    mylbl1.addStyleName("gwt-Font300");	

		this.button1 = new Button("Get XML"); 
		this.button3 = new Button("Get XML"); 
        this.button2 = new Button("Parse XML");
        this.button4 = new Button("Parse XML");
	    
		FlexTable tbl1 = new FlexTable();
		Label lbl1= new Label ("Patient ID:"), lbl2= new Label ("1234567");
		Label lbl3= new Label ("Patient Name:");
		Label lbl4= new Label ("**** ******");
		Label lbl5= new Label ("Patient Age:");
		Label lbl6= new Label ("32");
		Label lbl7= new Label ("Patient Gender:");
		Label lbl8= new Label ("Female");
		Label lbl9= new Label ("Diagnosis:");
		Label lbl10= new Label ("Human immunodeficiency virus (HIV)");
		Label lbl11= new Label ("Medication:");
		Label lbl12= new Label ("Stribild");
		
		tbl1.setWidget(0, 0, lbl1);
		tbl1.setWidget(0, 1, lbl2);
		tbl1.setWidget(1, 0, lbl3);
		tbl1.setWidget(1, 1, lbl4);	
		tbl1.setWidget(2, 0, lbl5);
		tbl1.setWidget(2, 1, lbl6);			
		tbl1.setWidget(3, 0, lbl7);
		tbl1.setWidget(3, 1, lbl8);	
		tbl1.setWidget(4, 0, lbl9);
		tbl1.setWidget(4, 1, lbl10);	
		tbl1.setWidget(4, 2, this.button1);	
		tbl1.setWidget(4, 3, this.button2);			
		tbl1.setWidget(5, 0, lbl11);
		tbl1.setWidget(5, 1, lbl12);	
		tbl1.setWidget(5, 2, this.button3);
		tbl1.setWidget(5, 3, this.button4);		
		vpanel1.add(tbl1);

		vpanel1.add(hpanel2);		
		hpanel1.add(vpanel1);
        button1.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event)
            {
            	Get_XML("/XMLs/HIV_XML.xml");
                
            }
        });
        button3.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event)
            {
            	Get_XML("/XMLs/HIVStribild.xml");
                
            }
        });
        button2.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event)
            {
                retrieve("/XMLs/HIV_XML.xml");
                
            }
        });
        button4.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event)
            {
                retrieve("/XMLs/HIVStribild.xml");
                
            }
        });
   		
    		this.img2= new Image("/IMAGES1/IMG_2881.JPG");

    		
    		this.mylbl3=new Label("Available Web Resources");
    	    mylbl3.addStyleName("gwt-Font300");			
    		vPanel2.add(this.mylbl3);

    }

    public void Get_XML( String url1)
    {


        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
                URL.encode(url1));

        try
        {
            builder.sendRequest(null, new RequestCallback() {
                public void onError(Request request, Throwable exception)
                {
                    // Couldn't connect to server (could be timeout, SOP
                    // violation, etc.)
                    Window.alert("Couldn't retrieve XML");
                }

                public void onResponseReceived(Request request,
                        Response response)
                {
                    if (200 == response.getStatusCode())
                    {
                        // Process the response in response.getText()
                		hpanel2.add(textarea1);
        	            textarea1.setText(response.getText());
        	            textarea1.setPixelSize(650, 600);
        	    		
                    }
                    else
                    {
                        // Handle the error. Can get the status text from
                        // response.getStatusText()
                        Window.alert("Error: " + response.getStatusText());
                    }
                }
            });
        }
        catch (RequestException e)
        {
            // Couldn't connect to server
            Window.alert("Couldn't connect to server to retrieve the XML: \n");
        }
    }

    public void retrieve(String url1)
    {

        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
                URL.encode(url1));

        try
        {
            builder.sendRequest(null, new RequestCallback() {
                public void onError(Request request, Throwable exception)
                {
                    // Couldn't connect to server (could be timeout, SOP
                    // violation, etc.)
                    Window.alert("Couldn't retrieve XML");
                }

                public void onResponseReceived(Request request,
                        Response response)
                {
                    if (200 == response.getStatusCode())
                    {
                        // Process the response in response.getText()
                        parseMessage(response.getText());
                    }
                    else
                    {
                        // Handle the error. Can get the status text from
                        // response.getStatusText()
                        Window.alert("Error: " + response.getStatusText());
                    }
                }
            });
        }
        catch (RequestException e)
        {
            // Couldn't connect to server
            Window.alert("Couldn't connect to server to retrieve the XML: \n");
        }
    }

    private void parseMessage(String messageXml)
    {
        try
        {
            // parse the XML document into a DOM
        	vPanel2.clear();
    		vPanel2.add(this.img2);
            Document messageDom = XMLParser.parse(messageXml);
            
            Element element = messageDom.getDocumentElement();
            
            NodeList feed_nlist = element.getElementsByTagName( "feed" );
            String string1=new String("Structure of the XML Document: \n\n ");
           
      
            for( int i = 0; i < feed_nlist.getLength(); ++i ) {
             	
                // get feed level
                Node feed_Node = feed_nlist.item(i);       	
                Element feed_Elmnt = (Element) feed_Node;
               	vPanel2.add(new Label(feed_Elmnt.getElementsByTagName("title").item(0).getFirstChild().getNodeValue()));                
                NodeList entry_nlist = feed_Elmnt.getElementsByTagName( "entry" ); 
               	string1=string1+"title: "+messageDom.getElementsByTagName("title").item(i).getFirstChild().getNodeValue()+"\n";
                
                for( int j = 0; j < entry_nlist.getLength(); ++j ) { 
                    // get entry level
                   Node entry_Node = entry_nlist.item(j);       	
                   Element entry_Elmnt = (Element) entry_Node;                	
               	   string1=string1+"    entry: "+entry_Elmnt.getElementsByTagName("title").item(0).getFirstChild().getNodeValue()+"\n";               	
               	   Node link_node = entry_Elmnt.getElementsByTagName("link").item(0);
               	   String href = ((Element)link_node).getAttribute("href");
               	   String title = ((Element)link_node).getAttribute("title");                
              	   HTML link = new HTML("<a  href=\"" + href +"\" target=\"_blank\">" + title + "</a>");
               	   vPanel2.add(link);              	
                }              
            
            }
            Label label1 = new Label (string1);
            label1.getElement().getStyle().setProperty("whiteSpace", "pre");

            hpanel1.add(vPanel2);
            Window.alert(string1);    

        }
        catch (DOMException e)
        {
            Window.alert("Could not parse XML document.");
        }
    }
}
	
